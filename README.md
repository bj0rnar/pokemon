# Pokemon
A Pokemon catalogue web app that displays Pokemon using the Pokemon API https://pokeapi.co/

### Participants
Bjørnar Pedersen and Amalie Hunsbedt

## Functionality
__Landing page:__
The user is first presented with the landing page where they can input their "Trainer name" which is stored locally. When the user returns to the web app, they are not asked for their trainer name again, but redirected to the Pokemon catalogue. The user does not have access to the landing page when 'logged in' and does not have access to any other page unless they have entered a trainer name.

__Trainer page:__
This is the user's profile page. All the Pokemon the Trainer has collected are displyed here. Each item is clickable and takes the user to the Pokemon Detail page for the specific Pokemon that was clicked.

__Pokemon catalogue:__
The catalogue displays all the Pokemon. Each Pokemon card is clickable and takes the user to the Pokemon Detail page.

__Pokemon detail page:__
This page displays an image of the chosen Pokemon along with its abilities, stats, height, weight, types and moves. Collect a Pokemon by clicking the "Collect" button.


### Technologies
- Angualr
- WebStorm
- LocalStorage
- PokeAPI
- Git


## Project

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.2.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
