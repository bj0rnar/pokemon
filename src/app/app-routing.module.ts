import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PokemondetailsComponent} from './details/components/pokemondetails.component';
import {PokemoncatalogueComponent} from './catalogue/components/pokemoncatalogue.component';
import {LoggedoutGuard} from './guards/loggedout/loggedout.guard';
import {LoginGuard} from './guards/login/login.guard';

const routes: Routes = [
  {
    path: 'trainer',
    loadChildren: () => import('./trainer/trainer.module').then(m => m.TrainerModule),
    canActivate: [LoggedoutGuard]
  },
  {
    path: 'details/:id',
    loadChildren: () => import('./details/details.module').then(me => me.DetailsModule),
    canActivate: [LoggedoutGuard]
  },
  {
    path: 'catalogue',
    loadChildren: () => import('./catalogue/catalogue.module').then(m => m.CatalogueModule),
    canActivate: [LoggedoutGuard]
  },
  {
    path: '',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
    canActivate: [LoginGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
