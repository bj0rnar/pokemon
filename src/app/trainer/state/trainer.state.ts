import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {TrainerModel} from '../model/trainer.model';

@Injectable({
  providedIn: 'root'
})
export class TrainerState {

  private readonly trainer$: BehaviorSubject<TrainerModel> = new BehaviorSubject<TrainerModel>(null);

  getTrainer$(): Observable<TrainerModel> {
    return this.trainer$.asObservable();
  }

  setTrainer(trainer: TrainerModel): void {
    return this.trainer$.next(trainer);
  }

}
