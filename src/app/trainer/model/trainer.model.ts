import {Pokemon} from '../../model/pokemon';

export interface TrainerModel {
  trainer: string;
  pokemon: Pokemon[];
}
