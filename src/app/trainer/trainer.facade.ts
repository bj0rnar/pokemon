import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import Database from '../database/Database';
import {TrainerState} from './state/trainer.state';
import {TrainerModel} from './model/trainer.model';

@Injectable({
  providedIn: 'root'
})
export class TrainerFacade {

  constructor(private readonly db: Database, private trainerState: TrainerState) {
  }

  loadCurrentUser(): void {
    this.trainerState.setTrainer(this.db.getCurrentUser());
  }
  getCurrentUser$(): Observable<TrainerModel> {
    return this.trainerState.getTrainer$();
  }
}
