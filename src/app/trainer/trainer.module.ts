import {NgModule} from '@angular/core';
import {TrainerComponent} from './components/trainer.component';
import {TrainerRoutingModule} from './trainer-routing.module';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    TrainerComponent
  ],
  imports: [
    TrainerRoutingModule,
    CommonModule
  ]
})

export class TrainerModule {}
