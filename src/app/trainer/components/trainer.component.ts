import { Component, OnInit } from '@angular/core';
import {TrainerFacade} from '../trainer.facade';
import {Observable} from 'rxjs';
import {TrainerModel} from '../model/trainer.model';

@Component({
  selector: 'app-trainerpage',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.scss']
})

/**
 * The Trainer component displays the trainer name and all collected pokemon
 * This page redirects to the login page if a user does not exist in the db
 */

export class TrainerComponent implements OnInit {

  constructor(private readonly trainerFacade: TrainerFacade) {
  }

  // load the current user
  ngOnInit(): void {
    this.trainerFacade.loadCurrentUser();
  }

  // observable object made available to the HTML
  getCurrentTrainer$(): Observable<TrainerModel> {
    return this.trainerFacade.getCurrentUser$();
  }

}
