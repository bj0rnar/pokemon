import { Component } from '@angular/core';
import Database from './database/Database';

// hello

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'pokemon';

  constructor(private readonly database: Database) {
  }

  // Method used for displaying navbar or not.
  checkLogin(): any {
    return (this.database.getCurrentUser());
  }
}
