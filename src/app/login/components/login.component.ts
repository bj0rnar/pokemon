import { Component, OnInit } from '@angular/core';
import Database from '../../database/Database';
import {Router} from '@angular/router';

/**
 * LoginComponent:
 * Contains a form which registers the user.
 * Is skipped by the login guard if there already exists
 * a user in the database
 */


@Component({
  selector: 'app-loginpage',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  db: Database;

  constructor(private readonly router: Router) {
    // get the current instance of the database.
    this.db = new Database();
  }

  ngOnInit(): void {
  }

  onButtonClick(): void {
    // Get the username from html input field.
    const trainerName = ((document.getElementById('userInput') as HTMLInputElement).value);
    // If the field is not empty, create a user by given name and navigate
    if (trainerName !== ''){
      this.db.createUser(trainerName);
      this.router.navigateByUrl('/trainer');
    }
    else {
      // Otherwise alert the user
      alert('No name detected');
    }
  }

}
