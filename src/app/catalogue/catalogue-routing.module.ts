import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {PokemoncatalogueComponent} from './components/pokemoncatalogue.component';

const routes = [
  {
    path: '',
    component: PokemoncatalogueComponent
  }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CatalogueRoutingModule {}
