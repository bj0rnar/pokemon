import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';

const pokemonUrl = 'https://pokeapi.co/api/v2';

@Injectable({
  providedIn: 'root'
})


/**
 * API Service used for extracting pokemons for the catalogue.
 * Takes LIMIT and OFFSET as parameters for switching components.
 */

export class CatalogueApi {

  constructor(private readonly http: HttpClient) { }

  // All the pokemons for the catalogue (set limit to ${UPPER_LIMIT}) for all
  public getAllPokemons(limit: string, offset: string): Observable<any> {
    return this.http.get(`${pokemonUrl}/pokemon?limit=${limit}&offset=${offset}`);
  }

}
