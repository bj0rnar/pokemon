import {NgModule} from '@angular/core';
import {PokemoncatalogueComponent} from './components/pokemoncatalogue.component';
import {CatalogueRoutingModule} from './catalogue-routing.module';
import {CommonModule} from '@angular/common';

/**
 * CommonModule for NgIf etc operations
 */


@NgModule({
  declarations: [
    PokemoncatalogueComponent
  ],
  imports: [
    CatalogueRoutingModule,
    CommonModule
  ]
})

export class CatalogueModule {}
