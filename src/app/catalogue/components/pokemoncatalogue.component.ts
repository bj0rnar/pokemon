import { Component, OnInit } from '@angular/core';
import { CatalogueFacade} from '../catalogue.facade';
import {Observable} from 'rxjs';
import {CatalogueModel} from '../model/catalogue.model';

@Component({
  selector: 'app-pokemoncatalogue',
  templateUrl: './pokemoncatalogue.component.html',
  styleUrls: ['./pokemoncatalogue.component.scss']
})

/**
 * PokemonCatalogue is built as an actual catalogue, where
 * 15 pokemon are shown on one page, and you switch pages
 * by doing additional API requests
 */


export class PokemoncatalogueComponent implements OnInit {

  // Class variables for html display
  offset = 0;
  public pageNr = 1;
  numOfPokemonsShown = 15;

  constructor(private readonly catalogueFacade: CatalogueFacade) {
    this.catalogueFacade.loadPokemons$(this.numOfPokemonsShown.toString(), this.offset.toString());
  }

  // Get observable on component load
  ngOnInit(): void {
    this.getPokemons$();
  }

  // Switch pages on click by increasing the offset and making another request
  onNextButtonClickHandling(): void {
    this.offset += 15;
    this.pageNr += 1;
    this.catalogueFacade.loadPokemons$(this.numOfPokemonsShown.toString(), this.offset.toString());
  }

  // Switch pages, but go back one page instead.
  onPrevButtonClickHandling(): void {
    this.offset -= 15;
    this.pageNr -= 1;
    this.catalogueFacade.loadPokemons$(this.numOfPokemonsShown.toString(), this.offset.toString());
  }

  // Observable object made available to the HTML
  getPokemons$(): Observable<CatalogueModel> {
     return this.catalogueFacade.getPokemonFromState$();
  }
}
