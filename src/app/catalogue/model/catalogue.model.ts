import {Pokemon} from '../../model/pokemon';

/**
 * Model used for Catalogue
 * Next and prev is given from the API and are kept for "if" conditions for
 * showing the next/previous page buttons in the html
 * pokemonArray contains pokemon from the API response
 */

export interface CatalogueModel {
  next: string;
  prev: string;
  pokemonArray: Pokemon[];
}
