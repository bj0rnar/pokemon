import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {CatalogueModel} from '../model/catalogue.model';

@Injectable({
  providedIn: 'root'
})
export class CatalogueState {

  private readonly pokemons$: BehaviorSubject<CatalogueModel> = new BehaviorSubject<CatalogueModel>(null);

  getPokemons$(): Observable<CatalogueModel> {
    return this.pokemons$.asObservable();
  }

  setPokemons(pokemons: CatalogueModel): void {
    return this.pokemons$.next(pokemons);
  }

}
