import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {CatalogueApi} from './api/catalogue.api';
import {CatalogueState} from './state/catalogue.state';
import {CatalogueModel} from './model/catalogue.model';

@Injectable({
  providedIn: 'root'
})
export class CatalogueFacade {

  constructor(private readonly catalogueAPI: CatalogueApi, private catalogueState: CatalogueState) {
  }

  // Return object from state as an Observable object
  getPokemonFromState$(): Observable<CatalogueModel>{
    return this.catalogueState.getPokemons$();
  }

  // Initialize API request
  loadPokemons$(limit: string, offset: string): void {
    this.catalogueAPI.getAllPokemons(limit, offset).subscribe(response => {
      // Custom object to keep both next, previous and all the pokemon
      const pokemonCatalogue: CatalogueModel = {
        next: response.next,
        prev: response.previous,
        pokemonArray: []
      };
      // Iterator for adding all pokemon to the custom object
      for (const res of response.results) {
        const id = res.url.substring(res.url.length - 4).match(/[0-9]/g).join('');
        pokemonCatalogue.pokemonArray.push({
          id,
          name: res.name,
          image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${id}.svg`
        });
      }
      // Once complete, bind the state to the custom object.
      this.catalogueState.setPokemons(pokemonCatalogue);
    });
  }
}
