import {NgModule} from '@angular/core';
import {PokemondetailsComponent} from './components/pokemondetails.component';
import {CommonModule} from '@angular/common';
import {DetailsRoutingModule} from './details-routing.module';

@NgModule({
  declarations: [
    PokemondetailsComponent
  ],
  imports: [
    DetailsRoutingModule,
    CommonModule
  ]
})

export class DetailsModule {}
