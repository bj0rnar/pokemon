import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Pokemon} from '../../model/pokemon';

@Injectable({
  providedIn: 'root'
})
export class DetailsState {

  private readonly pokemons$: BehaviorSubject<Pokemon> = new BehaviorSubject<Pokemon>(null);

  getPokemon$(): Observable<Pokemon> {
    return this.pokemons$.asObservable();
  }

  setPokemon$(pokemons: Pokemon): void {
    return this.pokemons$.next(pokemons);
  }

}
