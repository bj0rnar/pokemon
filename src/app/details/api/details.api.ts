import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';

/**
 * Service for getting a pokemon based on ID
 */


const pokemonUrl = 'https://pokeapi.co/api/v2';

@Injectable({
  providedIn: 'root'
})

export class DetailsApi {

  constructor(private readonly http: HttpClient) { }

  // Single pokemon for the details view ?
  public getPokemonById(id: string): Observable<any> {
    return this.http.get(`${pokemonUrl}/pokemon/${id}`);
  }

}
