import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {PokemondetailsComponent} from './components/pokemondetails.component';

const routes = [
  {
    path: '',
    component: PokemondetailsComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class DetailsRoutingModule {}
