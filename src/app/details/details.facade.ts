import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {DetailsApi} from './api/details.api';
import {DetailsState} from './state/details.state';
import {Pokemon} from '../model/pokemon';

@Injectable({
  providedIn: 'root'
})
export class DetailsFacade {

  constructor(private readonly detailsApi: DetailsApi, private detailsState: DetailsState) {
  }

  // Build the pokemon object based on the API response.
  loadPokemonById(id: string): void {
    this.detailsApi.getPokemonById(id).subscribe( data => {
      let pokemon: Pokemon;
      pokemon = {
        weight: data.weight,
        height: data.height,
        baseXp: data.base_experience,
        image: data.sprites.other.dream_world.front_default,
        types: data.types,
        stats: data.stats,
        name: data.name,
        moves: data.moves,
        abilities: data.abilities
      };
      // Set details-state to the built pokemon object
      this.detailsState.setPokemon$(pokemon);
    });
  }
  // Getter for the observable object
  getPokemonFromState$(): Observable<Pokemon> {
    return this.detailsState.getPokemon$();
  }
}
