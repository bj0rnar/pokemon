import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Data} from '@angular/router';
import {Pokemon} from '../../model/pokemon';
import Database from '../../database/Database';
import {DetailsFacade} from '../details.facade';
import {Observable, Subscription} from 'rxjs';


@Component({
  selector: 'app-pokemondetails',
  templateUrl: './pokemondetails.component.html',
  styleUrls: ['./pokemondetails.component.scss']
})
export class PokemondetailsComponent implements OnInit, OnDestroy {

  pokemonId = '';
  public selectPokemon: Pokemon;
  db: Database;
  private pokemon$: Subscription;

  constructor(private route: ActivatedRoute, public readonly detailsFacade: DetailsFacade) {
    // Retrieve id from header
    this.pokemonId = this.route.snapshot.paramMap.get('id');
    // Get the database instance
    this.db = new Database();
    // Deconstruct the Observable pokemon in order to save it in onButtonCollect
    this.pokemon$ = this.detailsFacade.getPokemonFromState$().subscribe(pokemon => {
      if (pokemon === null) { return; }
      this.selectPokemon = pokemon;
    });
  }

  // Get the pokemon on component load
  ngOnInit(): void {
    this.detailsFacade.loadPokemonById(this.pokemonId);
  }

  // Unsubscribe on component destroy
  ngOnDestroy(): void {
    this.pokemon$.unsubscribe();
  }

  // Exposing the Observable pokemon to the HTML
  getSelectedPokemon(): Observable<Pokemon> {
    return this.detailsFacade.getPokemonFromState$();
  }

  // Store the subscribed pokemon in the constructor in the database.
  onButtonCollect(): void {
    this.db.addPokemonToUserList({id: this.pokemonId, name: this.selectPokemon.name, image: this.selectPokemon.image});
  }
}
