import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import Database from '../../database/Database';

/**
 * Guard which prevents a logged in user from accessing the login screen.
 */


@Injectable({
  providedIn: 'root'
})

export class LoginGuard implements CanActivate {

  constructor(private db: Database, private router: Router) {
    this.db = new Database();
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    // Checks if the database contains a user. A single user per browser
    if (this.db.getCurrentUser() === null) {
      return true;
    }
    else {
      this.router.navigateByUrl('/catalogue');
      return false;
    }
  }
}
