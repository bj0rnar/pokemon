import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import Database from '../../database/Database';


/**
 * Guard which returns the user back to the login screen
 * Makes sure no unauthorized users are allowed to see trainer, catalogue or details
 */


@Injectable({
  providedIn: 'root'
})
export class LoggedoutGuard implements CanActivate {

  constructor(private db: Database, private router: Router) {
    this.db = new Database();
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    // Checks if the database contains a user. A single user per browser
    if (this.db.getCurrentUser() !== null) {
      return true;
    } else {
      this.router.navigateByUrl('');
      return false;
    }
  }
}
