/**
 * Global pokemon model used in several components.
 * Contains nullable values for flexibility.
 * Entire object matches the pokeapi API response for getById
 */

export interface Pokemon {
  id?: string;
  weight?: string;
  height?: string;
  baseXp?: string;
  image: string;
  types?: [];
  stats?: string;
  name: string;
  moves?: string[];
  abilities?: [];
}
