/**
 * Interface for setting the rules for what operations a database in this project
 * should be able to do.
 */

export interface IRepository {
  createUser(userId: string): void;
  getCurrentUser(): any;

  getUserPokemonList(userId: string): [];
  addPokemonToUserList(pokemonId: string): void;
}
