import {IRepository} from './IRepository';
import {Injectable} from '@angular/core';

/**
 * Repository pattern for database (in our case localStorage)
 * Class implements interface which determines which operations it must be
 * able to follow, thus making it easier to swap out databases without having
 * to refactor multiple files.
 */

// Singleton!
@Injectable({
  providedIn: 'root'
})
class Database implements IRepository {

  private myDb = localStorage;

  createUser(userName: string): void {
    this.myDb.setItem('user', JSON.stringify({ trainer: userName, pokemon: []}));
  }

  getCurrentUser(): any {
    return JSON.parse(this.myDb.getItem('user'));
  }

  getUserPokemonList(): [] {
    return JSON.parse(this.myDb.getItem('user')).pokemon;
  }

  addPokemonToUserList(pokemon: {}): void {
    const tmp = JSON.parse(this.myDb.getItem('user'));
    tmp.pokemon.push(pokemon);
    this.myDb.setItem('user', JSON.stringify(tmp));
  }
}

export default Database;
